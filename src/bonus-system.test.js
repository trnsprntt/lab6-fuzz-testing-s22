import {calculateBonuses} from "./bonus-system";
const assert = require("assert");


describe('Calculator tests', () => {
    let app;

    let less_10k = 20;
    let lower_boundary_10k = 9999;
    let exact_10k = 10000;
    let upper_boundary_10k = 10001;
    let between_10k_50k = 30000;
    let lower_boundary_50k = 49999;
    let exact_50k = 50000;
    let upper_boundary_50k = 50001;
    let between_50k_100k = 70000;
    let lower_boundary_100k = 99999;
    let exact_100k = 100000;
    let upper_boundary_100k = 100001;
    let more_100k = 200000;

    let standard = "Standard";
    let premium = "Premium";
    let diamond = "Diamond";
    let invalid = "Invalid";

    console.log("Tests started");


    test('standard',  (done) => {
        let program = standard;
        expect(calculateBonuses(program, less_10k)).toEqual(0.05 * 1);
        expect(calculateBonuses(program, lower_boundary_10k)).toEqual(0.05 * 1);
        expect(calculateBonuses(program, exact_10k)).toEqual(0.05 * 1.5);
        expect(calculateBonuses(program, upper_boundary_10k)).toEqual(0.05 * 1.5);
        expect(calculateBonuses(program, between_10k_50k)).toEqual(0.05 * 1.5);
        expect(calculateBonuses(program, lower_boundary_50k)).toEqual(0.05 * 1.5);
        expect(calculateBonuses(program, exact_50k)).toEqual(0.05 * 2);
        expect(calculateBonuses(program, upper_boundary_50k)).toEqual(0.05 * 2);
        expect(calculateBonuses(program, between_50k_100k)).toEqual(0.05 * 2);
        expect(calculateBonuses(program, lower_boundary_100k)).toEqual(0.05 * 2);
        expect(calculateBonuses(program, exact_100k)).toEqual(0.05 * 2.5);
        expect(calculateBonuses(program, upper_boundary_100k)).toEqual(0.05 * 2.5);
        expect(calculateBonuses(program, more_100k)).toEqual(0.05 * 2.5);
        done();
    });

    test('premium',  (done) => {
        let program = premium;
        expect(calculateBonuses(program, less_10k)).toEqual(0.1 * 1);
        expect(calculateBonuses(program, lower_boundary_10k)).toEqual(0.1 * 1);
        expect(calculateBonuses(program, exact_10k)).toEqual(0.1 * 1.5);
        expect(calculateBonuses(program, upper_boundary_10k)).toEqual(0.1 * 1.5);
        expect(calculateBonuses(program, between_10k_50k)).toEqual(0.1 * 1.5);
        expect(calculateBonuses(program, lower_boundary_50k)).toEqual(0.1 * 1.5);
        expect(calculateBonuses(program, exact_50k)).toEqual(0.1 * 2);
        expect(calculateBonuses(program, upper_boundary_50k)).toEqual(0.1 * 2);
        expect(calculateBonuses(program, between_50k_100k)).toEqual(0.1 * 2);
        expect(calculateBonuses(program, lower_boundary_100k)).toEqual(0.1 * 2);
        expect(calculateBonuses(program, exact_100k)).toEqual(0.1 * 2.5);
        expect(calculateBonuses(program, upper_boundary_100k)).toEqual(0.1 * 2.5);
        expect(calculateBonuses(program, more_100k)).toEqual(0.1 * 2.5);
        done();
    });

    test('diamond',  (done) => {
        let program = diamond;
        expect(calculateBonuses(program, less_10k)).toEqual(0.2 * 1);
        expect(calculateBonuses(program, lower_boundary_10k)).toEqual(0.2 * 1);
        expect(calculateBonuses(program, exact_10k)).toEqual(0.2 * 1.5);
        expect(calculateBonuses(program, upper_boundary_10k)).toEqual(0.2 * 1.5);
        expect(calculateBonuses(program, between_10k_50k)).toEqual(0.2 * 1.5);
        expect(calculateBonuses(program, lower_boundary_50k)).toEqual(0.2 * 1.5);
        expect(calculateBonuses(program, exact_50k)).toEqual(0.2 * 2);
        expect(calculateBonuses(program, upper_boundary_50k)).toEqual(0.2 * 2);
        expect(calculateBonuses(program, between_50k_100k)).toEqual(0.2 * 2);
        expect(calculateBonuses(program, lower_boundary_100k)).toEqual(0.2 * 2);
        expect(calculateBonuses(program, exact_100k)).toEqual(0.2 * 2.5);
        expect(calculateBonuses(program, upper_boundary_100k)).toEqual(0.2 * 2.5);
        expect(calculateBonuses(program, more_100k)).toEqual(0.2 * 2.5);
        done();
    });

    test('invalid',  (done) => {
        let program = invalid;
        expect(calculateBonuses(program, less_10k)).toEqual(0 * 1);
        expect(calculateBonuses(program, lower_boundary_10k)).toEqual(0 * 1);
        expect(calculateBonuses(program, exact_10k)).toEqual(0 * 1.5);
        expect(calculateBonuses(program, upper_boundary_10k)).toEqual(0 * 1.5);
        expect(calculateBonuses(program, between_10k_50k)).toEqual(0 * 1.5);
        expect(calculateBonuses(program, lower_boundary_50k)).toEqual(0 * 1.5);
        expect(calculateBonuses(program, exact_50k)).toEqual(0 * 2);
        expect(calculateBonuses(program, upper_boundary_50k)).toEqual(0 * 2);
        expect(calculateBonuses(program, between_50k_100k)).toEqual(0 * 2);
        expect(calculateBonuses(program, lower_boundary_100k)).toEqual(0 * 2);
        expect(calculateBonuses(program, exact_100k)).toEqual(0 * 2.5);
        expect(calculateBonuses(program, upper_boundary_100k)).toEqual(0 * 2.5);
        expect(calculateBonuses(program, more_100k)).toEqual(0 * 2.5);
        done();
    });

    console.log('Tests Finished');

});
